#
# Be sure to run `pod lib lint NexIDSDK.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'NexIDSDK'
  s.version          = '1.4.0'
  s.summary          = 'EKYC Module built by Nexstream SDN BHD'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

#   s.description      = <<-DESC
# TODO: Add long description of the pod here.
#                        DESC

  s.swift_version = '5.0'
  s.pod_target_xcconfig = { 'IPHONEOS_DEPLOYMENT_TARGET' => '13.0' }
  s.user_target_xcconfig = { 'IPHONEOS_DEPLOYMENT_TARGET' => '13.0' }

  s.homepage         = 'https://gitlab.com/nexstream-ios/konnectedscansdk-cocoapod'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Chris Cheng' => 'chris@appxtream.com' }
  s.source           = { :git => 'https://gitlab.com/nexstream-ios/konnectedscansdk-cocoapod.git', :tag => "#{s.version}" }

  s.platform = :ios, '13.0'
  s.ios.deployment_target = '13.0'
  
  s.ios.vendored_frameworks = "NexIDSDK.framework","MGFaceIDBaseKit.framework",'MGFaceIDLiveDetect.framework'
  s.resources = "Resources/**/*"
  s.resource_bundles = {
    "MGFaceIDLiveCustomDetect" => ["Resources/MGFaceIDLiveCustomDetect.bundle"]
  }  
  # s.public_header_files = 'Pod/Classes/**/*.h'
  s.frameworks = 'SystemConfiguration', 'WebKit', 'CoreMotion', 'CoreMedia', 'AVFoundation', 'MediaPlayer'
  s.static_framework = true
  s.dependency 'Alamofire', '5.7.1'
end
