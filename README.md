# NexIDSDK

## Requirements
1. Minimum iOS Deployment - iOS 13

## Installation

To install it, simply add the following line to your Podfile:

```ruby
pod 'NexIDSDK', :git => 'https://gitlab.com/nexstream-ios/konnectedscansdk-cocoapod.git', :tag => '1.4.0'
```

For podspec, you should refer to this below :
```ruby
    s.platform     = :ios, "13.0"

    s.ios.deployment_target = "13.0"
  
    s.pod_target_xcconfig = { 'IPHONEOS_DEPLOYMENT_TARGET' => '13.0' }
    s.user_target_xcconfig = { 'IPHONEOS_DEPLOYMENT_TARGET' => '13.0' }

    s.resources = "Resources/**/*"
    s.resource_bundles = {
        'MGFaceIDLiveCustomDetect' => ['Resources/MGFaceIDLiveCustomDetect.bundle']
    }

    s.vendored_frameworks = "NexIDSDK.framework","MGFaceIDLiveDetect.framework","MGFaceIDBaseKit.framework"

    s.frameworks = 'SystemConfiguration', 'WebKit', 'CoreMotion', 'CoreMedia', 'AVFoundation', 'MediaPlayer'

    s.static_framework = true
    s.dependency 'Alamofire', '5.7.1'
```

## Example
1. Implement `NexIDDelegate` to your class

*Swift*
```swift
import NexIDSDK

public class MainActivity: NexIDDelegate {
    func successResultOnDocumentScan(data: EkycResult) {
        debugPrint(data)
    }
    
    func failureResultOnScan() {
        debugPrint("Failed")
    }
    
    func successResultOnLivenessScan(data: EkycResult) {
        debugPrint(data)
    }
    
    
    func successResultOnBarcodeScan(data: String?) {
        debugPrint(data ?? "")
    }
}
```

```swift
// must init before start the scanning
// please allow few seconds for this to complete the process
// suggest this to be performed at very beginning of the activity
NexIDSDK.shared.initSDK(apiKey: apiKey, appId: appId)

// supported document - identity_card, password
NexIDSDK.shared.startDocumentScan(viewController, self, documentType: DocumentType.identity_card)

// supported livenessType - "still" , "meglive" , "flash"
NexIDSDK.shared.startLiveness(self, livenessType: livenessType, documentImageBase64: referenceImageBase64)
```

*Objective-C*
```objc
#import <NexIDSDK/NexIDSDK-Swift.h>

@interface TheViewController ()<NexIDDelegate>

- (void)failureResultOnScan { 
    
}

- (void)successResultOnBarcodeScanWithData:(NSString * _Nullable)data { 
    
}

- (void)successResultOnDocumentScanWithData:(EkycResult * _Nonnull)data { 
    
}

- (void)successResultOnLivenessScanWithData:(EkycResult * _Nonnull)data { 
    
}
```

```objc
// must init before start the scanning
// please allow few seconds for this to complete the process
// suggest this to be performed at very beginning of the activity
[[NexIDSDK shared] initSDKWithApiKey:@"apiKey" appId:@"appId"];

// supported document - identity_card, password
[[NexIDSDK shared] startDocumentScan:self :self documentType:DocumentTypeIdentity_card];

// supported livenessType - "still" , "meglive" , "flash"
[[NexIDSDK shared] startLiveness:self livenessType:@"still" documentImageBase64:@"base64"];
```

## Reference
Possible Liveness Result (Status Code and Message)
- 1000 : Live verification passed, comparison passed. (Will deduct balance)
- 2000 : Live verification passed, comparison failed. (Will deduct balance)
- 3000 : Reference data error, possible reasons: no such ID number, photo format error, face not found in the photo, etc. (Will deduct balance)
- 400 : No face found on the reference image. (Will not deduct balance)


## Author

Chris Cheng, chris@appxtream.com
